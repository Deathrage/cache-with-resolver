import CacheControl from '.';
import { expect } from 'chai';

let sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));

describe('Test cache with resolver', () => {
	it('Size after basic set', () => {
		let cacheControl = new CacheControl();
		cacheControl.set('foo', 'bar');
		expect(cacheControl.length).to.equal(1);
	});
	it('Get back basic set', () => {
		let cacheControl = new CacheControl();
		cacheControl.set('foo', 'bar');
		expect(cacheControl.get('foo')).to.equal('bar');
	});
	it('Get non present', () => {
		let cacheControl = new CacheControl();
		cacheControl.set('foo', 'bar');
		expect(cacheControl.get('moo')).to.equal(null);
	});
	it('Get all entries', () => {
		let cacheControl = new CacheControl();
		cacheControl.set('foo', 'bar');
		cacheControl.set('moo', 'mar');
		expect(cacheControl.entries).to.eql([
			['foo', 'bar'],
			['moo', 'mar'],
		]);
	});
	it('Cache deleteion', () => {
		let cacheControl = new CacheControl();
		cacheControl.set('foo', 'bar');
		cacheControl.delete('foo');
		expect(cacheControl.length).to.equal(0);
	});
	it('Cache filtered deletion', async () => {
		let cacheControl = new CacheControl();

		cacheControl.set('oldest', 'survive');
		await sleep(100);
		let olderThan = new Date();
		await sleep(100);
		cacheControl.set('middle', 'survive');
		await sleep(100);
		let newerThan = new Date();
		await sleep(100);
		cacheControl.set('newest', 'wont survive');

		cacheControl.filteredDelete({
			newerThan: newerThan,
			olderThan: olderThan,
		});

		expect(cacheControl.length).to.equal(1);
	});
	it('Cache drop', () => {
		let cacheControl = new CacheControl();
		cacheControl.set('foo', 'bar');
		cacheControl.drop();
		expect(cacheControl.length).to.equal(0);
	});
	it('True has', () => {
		let cacheControl = new CacheControl();
		cacheControl.set('foo', 'bar');
		expect(cacheControl.has('foo')).to.equal(true);
	});
	it('False has', () => {
		let cacheControl = new CacheControl();
		cacheControl.set('mat', 'bar');
		expect(cacheControl.has('moo')).to.equal(false);
	});

	it('Cache timeout default', async () => {
		let cacheControl = new CacheControl({
			timeToLive: 0.25,
		});
		cacheControl.set('foo', 'bar');
		await sleep(500);
		expect(cacheControl.get('foo')).to.equal(null);
	});
	it('Cache length after timeout', async () => {
		let cacheControl = new CacheControl({
			timeToLive: 0.25,
		});
		cacheControl.set('foo', 'bar');
		await sleep(500);
		expect(cacheControl.length).to.equal(0);
	});
	it('Cache entries after timeout', async () => {
		let cacheControl = new CacheControl({
			timeToLive: 0.25,
		});
		cacheControl.set('foo', 'bar');
		await sleep(500);
		expect(cacheControl.entries).to.eql([]);
	});

	it('Cache timeout override', async () => {
		let cacheControl = new CacheControl({
			timeToLive: 1,
		});
		cacheControl.set('foo', 'bar', 0.25);
		await sleep(500);
		expect(cacheControl.get('foo')).to.equal(null);
	});

	it('With custom Map', () => {
		let map = new Map();
		let cacheControl = new CacheControl({
			cacheMap: map,
		});
		cacheControl.set('foo', 'bar');
		expect(map.size).to.equal(1);
	});

	it('Get outdated cache', async () => {
		let cacheControl = new CacheControl({
			timeToLive: 0.1,
		});
		cacheControl.set('foo', 'bar');
		await sleep(250);
		expect(cacheControl.get('foo')).to.equal(null);
	});

	// Resolver
	it('sync resolver', () => {
		let cacheControl = new CacheControl();
		cacheControl.resolveSync('foo', cs => cs('bar'));
		expect(cacheControl.get('foo')).to.equal('bar');
	});
	it('sync resolver returner', () => {
		let cacheControl = new CacheControl();
		let val = cacheControl.resolveSync('foo', cs => cs('bar'));
		expect(val).to.equal('bar');
	});
	it('sync repeated resolve', () => {
		let cacheControl = new CacheControl();
		cacheControl.resolveSync('foo', cs => cs('bar'));
		let val = cacheControl.resolveSync('foo', cs => cs('bar'));
		expect(val).to.equal('bar');
	});
	it('async resolver', async () => {
		let cacheControl = new CacheControl();
		await cacheControl.resolve<string>('foo', cs => {
			setTimeout(() => {
				cs('bar');
			}, 250);
		});
		expect(cacheControl.get('foo')).to.equal('bar');
	});
	it('async resolver return', async () => {
		let cacheControl = new CacheControl();
		let val = await cacheControl.resolve<string>('foo', cs => {
			setTimeout(() => {
				cs('bar');
			}, 250);
		});
		expect(val).to.equal('bar');
	});
	it('async repeated resolve', async () => {
		let cacheControl = new CacheControl();
		await cacheControl.resolve<string>('foo', cs => {
			setTimeout(() => {
				cs('bar');
			}, 100);
		});
		let val = await cacheControl.resolve<string>('moo', cs => {
			setTimeout(() => {
				cs('bar');
			}, 100);
		});
		expect(val).to.equal('bar');
	});
});
