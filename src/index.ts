import cloneDeep = require('clone-deep');

export interface Cache {
	timeout: number;
	value: any;
	createdAt: number;
}
export interface CacheControlConfig {
	timeToLive?: number,
	cacheMap?: Map<any, any>
}
export default class CacheControl {
	private cacheMap: Map<any, Cache>;

	constructor(private config: CacheControlConfig = {}) {
		if (this.config.cacheMap) this.cacheMap = this.config.cacheMap;
		else this.cacheMap = new Map();
	}

	// triger filtration => operates with whole Map
	get length() {
		this.filterTTL();
		return this.cacheMap.size;
	}
	get entries(): [any, any][] {
		this.filterTTL();
		return Array.from(this.cacheMap.entries()).map(ar => {
			return [ar[0], ar[1].value];
		});
	}

	// Doesnt trigger => deletion operation do not return values

	drop(): void {
		this.cacheMap.clear();
	}

	filteredDelete(filter: {
		newerThan?: Date,
		olderThan?: Date
	}) {
		let entries = Array.from(this.cacheMap.entries());
		entries.forEach(pair => {
			let [key, cache] = pair;
			// check older
			if (filter.newerThan) {
				if (filter.newerThan.getTime() < cache.createdAt) this.delete(key);
			}
			if (filter.olderThan) {
				if (filter.olderThan.getTime() > cache.createdAt) this.delete(key);
			}
		});
	}

	delete(key: any): void {
		this.cacheMap.delete(key);
	}

	// Trigger resolve for specific key => returns value for that key
	has(key: any): boolean {
		this.resolveTTL(key);
		return this.cacheMap.has(key);
	}

	get(key: any): any {
		if (!this.has(key)) return null;
		let cache = this.resolveTTL(key);
		let value = cache ? cache.value : null;
		if (typeof value === 'object') value = cloneDeep(value);
		return value;
	}

	// Doesnt trigger => seting does not require checking for TTL as it overrides
	set(key: any, value: any, timeToLive?: number): void {
		let ttl = Number(timeToLive) || Number(this.config.timeToLive) || 3600;
		let ttlMS = ttl * 1000;
		let unixTime = (new Date()).getTime();
		let cache = {
			value: value,
			createdAt: unixTime,
			timeout: unixTime + ttlMS,
		} as Cache;
		this.cacheMap.set(key, cache);
	}

	// Resolvers

	/**
	 * Resolves function in async, for functions that include promises or anything async
	 */
	async resolve<T>(key: string, func: (cache: (val: T) => void) => void, timeToLive?: number): Promise<T> {
		// Try if cache exists and reutnr it
		let cachedVal = this.get(key);
		if (cachedVal) return cachedVal;
		// Resolve function
		let resProm = new Promise<T>(resolve => {
			const resolver = (val: any) => {
				resolve(val);
			};
			func(resolver);
		});
		let resVal = await resProm;
		// set new cache and return value
		this.set(key, resVal, timeToLive);
		if (typeof resVal === 'object') resVal = cloneDeep(resVal);
		return resVal;
	}

	/**
	 * Resolves function in sync, for function that do not include promises or anything async
	 */
	resolveSync<T>(key: string, func: (cache: (val: T) => void) => void, timeToLive?: number): T {
		// Try if cache exists and reutnr it
		let cachedVal = this.get(key);
		if (cachedVal) return cachedVal;
		let resVal = null;
		const resolver = (val: any) => {
			resVal = val;
		};
		func(resolver);
		// set new cache and return value
		this.set(key, resVal, timeToLive);
		if (typeof resVal === 'object') resVal = cloneDeep(resVal);
		return resVal;
	}

	// Private

	private resolveTTL(key: string, cache?: Cache) {
		// try resolve cache if not passed
		if (!cache) {
			cache = this.cacheMap.get(key);
			if (!cache) return;
		}

		if (cache.timeout < (new Date()).getTime()) {
			this.delete(key);
			return null;
		}

		return cache;
	}

	private filterTTL() {
		for (let entry of Array.from(this.cacheMap.entries())) {
			this.resolveTTL(entry[0], entry[1]);
		}
	}
}
